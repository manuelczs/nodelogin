const express = require('express');
const router = express();
const loginCtrl = require('../controllers/login.controller');

router.use('/login', loginCtrl);

router.get('/', (req, res) => {
  res.send('Hello from get');
})

module.exports = router;