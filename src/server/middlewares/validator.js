const validator = (req, res, next) => {
  if(req.body.user && req.body.password) {
    let user = req.body.user;
    let password = req.body.password;
    res.status(200).send(`User: ${user}, Password: ${password}`)
  } else {
    next();
  }
}

module.exports = validator