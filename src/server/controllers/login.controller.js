const router = require('express').Router();
const validator = require('../middlewares/validator');
const tokenValidator = require('../middlewares/tokenValidator');

router.route('/')
  .get(tokenValidator, (req, res) => {
    res.send('No Authorized')
  })
  .post(validator, (req, res) => {
    res.send('No valid login.');
  })

module.exports = router;