const app = require('./server');
const port = 3000;

const init = async () => {
  await app.listen(port, () => {
    console.log(`Port listening: ${port}`)
  });
}

init();